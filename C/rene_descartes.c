
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <pthread.h>
#include <semaphore.h> //libreria

static void *pensar(void *arg);
static void *lavar_los_platos(void *arg);
static void *limpiar_el_piso(void *arg);
static void *regar_las_plantas(void *arg);
static void *existir(void *arg);
static void *hablar(void *arg);
static void *decidir(void *arg);

pthread_t  pthread_1, pthread_2, pthread_3, pthread_4, pthread_5, pthread_6, pthread_7;

sem_t A;
sem_t B;
sem_t C;
sem_t D;
sem_t E;
sem_t ciclar;

int ciclos;

static void* pensar(void* arg)
{
	while(ciclos > 0)
	{
		printf("\nciclo %d\n", ciclos);
		sem_wait(&A);
		printf("Pienso\n");
		sem_post(&B);
		sem_post(&B);
		sem_post(&B);
		sem_post(&D);
		sem_wait(&ciclar);
	}
}

static void *lavar_los_platos(void *arg)
{
	while(ciclos > 0)
	{
		sem_wait(&A);
		sem_wait(&B);
		printf("Mientras lavo los platos, ");
		sem_wait(&ciclar);
	}
}

static void *limpiar_el_piso(void *arg)
{
	while(ciclos > 0)
	{
		sem_wait(&A);
		sem_wait(&B);
		printf("Mientras limpio el piso, ");
		sem_wait(&ciclar);
	}
}

static void *regar_las_plantas(void *arg)
{
	while(ciclos > 0)
	{
		sem_wait(&A);
		sem_wait(&B);
		printf("Mientras riego las plantas, ");
		sem_wait(&ciclar);
	}
}

static void *existir(void *arg)
{
	while(ciclos > 0)
	{
		sem_wait(&D);
		printf("\nExisto!\n");
		sem_post(&E);
		sem_post(&E);
		sem_wait(&ciclar);
	}
}

static void *hablar(void *arg)
{
	while(ciclos > 0)
	{
		sem_wait(&A);
		sem_wait(&E);
		printf("Hablar, ");
		sem_wait(&ciclar);
	}
}

static void *decidir(void *arg)
{
	while(ciclos > 0)
	{
		sem_wait(&A);
		sem_wait(&E);
		printf("Tomar una decisión\n");
		sem_post(&A);
		sem_post(&A);
		sem_post(&A);
		sem_post(&A);
		sem_post(&A);
		sem_post(&A);
		ciclos--;
		sem_post(&ciclar);
		sem_post(&ciclar);
		sem_post(&ciclar);
		sem_post(&ciclar);
		sem_post(&ciclar);
		sem_post(&ciclar);
	}
}

int main (void) 
{
	printf("Ingrese la cantidad de ciclos que desea que se ejecuten:\n");
	scanf("%d", &ciclos);

	sem_init(&A, 0, 6);
	sem_init(&B, 0, 0);
	sem_init(&C, 0, 0);
	sem_init(&D, 0, 0);
	sem_init(&E, 0, 0);
	sem_init(&ciclar, 0, 0);

        pthread_create(&pthread_1, NULL, *pensar, NULL);
	pthread_create(&pthread_2, NULL, *lavar_los_platos, NULL);
	pthread_create(&pthread_3, NULL, *limpiar_el_piso, NULL);
	pthread_create(&pthread_4, NULL, *regar_las_plantas, NULL);
	pthread_create(&pthread_5, NULL, *existir, NULL);
	pthread_create(&pthread_6, NULL, *hablar, NULL);
	pthread_create(&pthread_7, NULL, *decidir, NULL);

        pthread_join(pthread_1, NULL);
        pthread_join(pthread_2, NULL);
	pthread_join(pthread_3, NULL);
	pthread_join(pthread_4, NULL);
	pthread_join(pthread_5, NULL);
	pthread_join(pthread_6, NULL);
	pthread_join(pthread_7, NULL);

	sem_destroy(&A);
	sem_destroy(&B);
	sem_destroy(&C);
	sem_destroy(&D);
	sem_destroy(&E);
	sem_destroy(&ciclar);

	return 0;
}


