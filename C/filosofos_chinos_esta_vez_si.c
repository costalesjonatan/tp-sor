# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <pthread.h>
#include <semaphore.h>

static void *ciclo();
static void pensar(int id);
static void comer(int id, int izquierda, int derecha);
static void agarrarPalillos(int id, int izquierda, int derecha);
static void comprobar(int id, int izquierda, int derecha);
static void do_nothing(int microsegundos, char* mensaje);

sem_t mutex;
sem_t palillos[5];

int id;
int ciclos;

char* msg1 = "Eureka!";
char* msg2 = "Provecho!";

int main()
{
	id = -1;
	sem_init(&mutex,0,1);
	pthread_t filosofo[5];
	
	for(int i = 0; i < 5; i++)
	{
		sem_init(&palillos[i],0,1);
	}

	for(int i = 0; i < 5; i++)
	{
		pthread_create(&filosofo[i], NULL, *ciclo, NULL);
	}
	
	for(int i = 0; i < 5; i++)
	{
		pthread_join(filosofo[i], NULL);
	}
	
	return 0;
}

void *ciclo()
{
	id  += 1;
	ciclos = 0;
	int idFilosofo = id;

	int palilloIzquierdo, palilloDerecho;

	if(id == 0)
	{
		palilloIzquierdo = 4;
		palilloDerecho = idFilosofo;
	}
	else
	{
		palilloDerecho = idFilosofo;
		palilloIzquierdo = idFilosofo - 1;
	}

	while(ciclos < 20)
	{
		pensar(idFilosofo);
		agarrarPalillos(idFilosofo, palilloIzquierdo, palilloDerecho);
		comer(idFilosofo, palilloIzquierdo, palilloDerecho);
		sem_wait(&mutex);
		ciclos++;
		sem_post(&mutex);
	}
}

void pensar(int idf)
{
	printf("Soy el filosofo %d y estoy pensando...\n", idf+1);
	do_nothing(2000000, msg1);
	printf(" soy el filosofo %d\n", idf+1);
}

void comer(int idf, int izquierda, int derecha)
{
	printf("Soy el filosofo %d y estoy comiendo...\n", idf+1);
	do_nothing(2000000, msg2);
	printf("soy el filosofo %d\n", idf+1);
	sem_wait(&mutex);
	printf("Estoy liberando los palillos soy %d\n", idf+1);
	sem_post(&palillos[izquierda]);
	sem_post(&palillos[derecha]);
	sem_post(&mutex);
}

void do_nothing(int microsegundos, char* mensaje)
{
	usleep(microsegundos);
	printf(" %s ", mensaje);
}

void agarrarPalillos(int idf, int izquierda, int derecha)
{
	comprobar(idf, izquierda, derecha);
}

void comprobar(int idf, int izquierda, int derecha)
{
	int filosofoDerecho, filosofoIzquierdo;
	if(idf == 0)
	{
		filosofoIzquierdo = 4;
		filosofoDerecho = idf + 1;
	}
	else
	{
		filosofoIzquierdo = idf - 1;
		filosofoDerecho = idf + 1;
	}

	sem_wait(&palillos[izquierda]);
	printf("Soy el filosofo %d y estoy agarrando el palillo izquierdo...\n", idf+1);
	sem_wait(&palillos[derecha]);
	printf("Soy el filosofo %d y estoy agarrando el palillo derecho...\n", idf+1);
}

