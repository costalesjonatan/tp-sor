#!/bin/bash
# ------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------ setaf para color
#de letras/setab: color de fondo
	red=`tput setaf 1`;
	green=`tput setaf 2`;
	blue=`tput setaf 4`;
	bg_blue=`tput setab 4`;
	reset=`tput sgr0`;
	bold=`tput setaf bold`;
#------------------------------------------------------
# TODO: Completar con su path
#------------------------------------------------------
proyectoActual="/home/grupo1/TP-SOR"
#------------------------------------------------------
# DISPLAY MENU ------------------------------------------------------
imprimir_menu () {
       imprimir_encabezado "\t S U P E R - M E N U ";

    echo -e "\t\t El proyecto actual es:";
    echo -e "\t\t $proyectoActual";
 
    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  Ver estado del proyecto";
    echo -e "\t\t\t b.  Guardar cambios";
    echo -e "\t\t\t c.  Actualizar repo";
    echo -e "\t\t\t f.  Abrir en terminal";
    echo -e "\t\t\t g.  Abrir en carpeta";
    echo -e "\t\t\t h,  Do nothig";
    echo -e "\t\t\t i,  Filosofos chinos";
    echo -e "\t\t\t j,  Rene Descartes";
    echo -e "\t\t\t k,  carpetas del proyecto";
    echo -e "\t\t\t l,  archivos del proyecto";
    echo -e "\t\t\t m,  buscar filosofos";
    echo -e "\t\t\t n,  Carpetas y archivos del proyecto";
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}
#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------
imprimir_encabezado () {
    clear;
    #Se le agrega formato a la fecha que muestra Se agrega variable
    #$USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t\t USERNAME:$USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t ${bg_blue} ${red}${bold}--------------------------------------\t${reset}";
    echo -e "\t\t ${bold}${bg_blue}${red}$1\t\t${reset}";
    echo -e "\t\t ${bg_blue}${red} ${bold}--------------------------------------\t${reset}";
    echo "";
}
esperar () {
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}
malaEleccion () {
    echo -e "Selección Inválida ..." ;
}
decidir () {
	echo $1;
	while true; do
		echo "desea ejecutar? (s/n)";
    		read respuesta;
    		case $respuesta in
        		[Nn]* ) break;;
       			[Ss]* ) eval $1
				break;;
        		* ) echo "Por favor tipear S/s ó N/n.";;
    		esac
	done
}
#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------
a_funcion () {
    	imprimir_encabezado "\tOpción a.  Ver estado del proyecto";
	echo "---------------------------";
	echo "Somthing to commit?";
        decidir "cd $proyectoActual; git status";
        echo "---------------------------";
	echo "Incoming changes (need a pull)?";
	decidir "cd $proyectoActual; git fetch origin";
	decidir "cd $proyectoActual; git log HEAD..origin/master --oneline";
}
b_funcion () {
       	imprimir_encabezado "\tOpción b.  Guardar cambios";
       	decidir "cd $proyectoActual; git add -A";
       	echo "Ingrese mensaje para el commit:";
       	read mensaje;
       	decidir "cd $proyectoActual; git commit -m \"$mensaje\"";
       	decidir "cd $proyectoActual; git push";
}
c_funcion () {
      	imprimir_encabezado "\tOpción c.  Actualizar repo";
      	decidir "cd $proyectoActual; git pull";
}
f_funcion () {
	imprimir_encabezado "\tOpción f.  Abrir en terminal";
	decidir "cd $proyectoActual; xterm &";
}
g_funcion () {
	imprimir_encabezado "\tOpción g.  Abrir en carpeta";
	decidir "gnome-open $proyectoActual &";
}
#------------------------------------------------------
# TODO: Completar con el resto de ejercicios del TP, una funcion por
#cada item ------------------------------------------------------
chekearRepo () {
	clear;
	git fetch origin
	var=$(git log HEAD..origin/master --oneline)
	if [ -z "$var" ]; then
		echo "Su proyecto esta actualizaddo"
		esperar
	else
		echo "Necesita hacer un pull antes de comenzar a trabajar"
		esperar
		c_funcion
	fi
}

do_nothing(){
	cd; 
	cd TP-SOR/C;
	gcc do_nothing_secuencial.c -o do_nothing_secuencial;
	echo "Tiempo sin utilizar fork";
	time ./do_nothing_secuencial;
	gcc do_nothing_secuencial_fork.c -o do_nothing_secuencial_fork;
	echo "Tiempo utilizando fork";
	time ./do_nothing_secuencial_fork;
}

filosofos_chinos(){
	cd;
	cd TP-SOR/C;
	gcc filosofos_chinos_esta_vez_si.c -o filosofos_chinos_esta_vez_si -lpthread;
	./filosofos_chinos_esta_vez_si; 
}

rene_descartes(){
	cd;
	cd TP-SOR/C;
	gcc rene_descartes.c -o rene_descartes -lpthread;
	./rene_descartes;
}

carpetas(){
	cd;
	cd TP-SOR;
	ls > /home/grupo1/TP-SOR/otros/carpetas.txt;
	cd;
	decidir "cd TP-SOR/otros; nano -c carpetas.txt";
}

archivos(){
	cd;
	cd TP-SOR/C;
	ls -a > /home/grupo1/TP-SOR/otros/archivos.txt;
	cd;
	cd TP-SOR/bash
	ls -a >> /home/grupo1/TP-SOR/otros/archivos.txt;
	cd;
	cd TP-SOR/otros;
	ls -a >> /home/grupo1/TP-SOR/otros/archivos.txt;
	decidir "cd TP-SOR/otros; nano -c archivos.txt";
}

archivos_c(){
	cd;
	cd TP-SOR/otros;
	grep filosofos archivos.txt > /home/grupo1/TP-SOR/otros/archivos_c.txt;
	cd;
	decidir "cd TP-SOR/otros; nano -c archivos_c.txt";
}

proyecto_completo(){
	cd;
	cd TP-SOR/otros;
	cat carpetas.txt archivos.txt > carpetasYarchivos.txt;
	cd;
	decidir "cd TP-SOR/otros; nano - carpetasYarchivos.txt"; 
}



#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------
chekearRepo
while true; do

    # 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;

    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        e|E) e_funcion;;
        f|F) f_funcion;;
        g|G) g_funcion;;
	h|H) do_nothing;;
	i|I) filosofos_chinos;;
	j|J) rene_descartes;;
	k|K) carpetas;;
	l|L) archivos;;
	m|M) archivos_c;;
	n|N) proyecto_completo;;
        q|Q) break;;
        *) malaEleccion;;
    esac
    esperar; 
done
