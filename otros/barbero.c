#include<pthread.h>
#include<semaphore.h>
#include<stdio.h>

static void * pthread_1_funcion(void *arg);
static void * pthread_2_funcion(void *arg);

sem_t sem1,sem2;
int cant_clientes;
int cant_sillas;
char estado_barber;

int main (void)
{

cant_clientes=0;
cant_sillas=20;
sem_init(&sem1, 0, 1);
sem_init(&sem2, 0, 0);
pthread_t  pthread_barber, pthread_cliente;
        pthread_create(&pthread_barber, NULL, *pthread_1_funcion, NULL);
        pthread_create(&pthread_cliente, NULL, *pthread_2_funcion, NULL);

        pthread_join( pthread_barber, NULL);
        pthread_join( pthread_cliente, NULL);

return 0;

}

static void * pthread_1_funcion(void *arg)
{
        while(1)
        {
                if(cant_clientes==0)
                {
                // d = durmiendo
                estado_barber='d';
                printf("ZZZZ");
                }else{
                     sem_wait(&sem1);
                     cant_clientes--;
                     sem_post(&sem2);
                // c = Cortando pelo
                     estado_barber='c';
                     printf("Ocupado \n");
                      } 
                       printf("Gracias por el corte \n");
        }

}

static void * pthread_2_funcion(void *arg)
{
while (1)
{

  if(cant_clientes < cant_sillas)
  {
      //hay lugar, puedo entrar 
      cant_clientes++;
         printf("Hola, soy el cliente: %i) \n",cant_clientes);
      if(estado_barber=='c')
      {
         //esperar en la silla
         printf("leyendo revista...");

     }sem_wait(&sem2);
	      if (estado_barber=='d')
              {
         //despertar al barber
         printf("ejem, ejem.., cof, cof...");
              }
       sem_post(&sem1);
    }else{
      //no hay lugar, me voy
      sem_wait(&sem2);
      printf("chau $ chau $... \n");
      sem_post(&sem1);
         }
}
}


