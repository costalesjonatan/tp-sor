#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>

void do_nothing(int microsegundos, char* mensaje)
{
	usleep(microsegundos);
	printf("\n %s ", mensaje);
}

void do_nothing_random(char * mensaje)
{
	srand(time(NULL));
	int microsegundos = rand() % 1000 + 1;
	usleep(microsegundos);
	printf("\n %s ", mensaje);
}

int main(int argc, char *argv[])
{
	pid_t pid;
	int i = 0;
	char* msg = "Hola ";

	pid = fork();

	while(i < 10)
	{
		if(pid == 0)
		{
			do_nothing(2000000, msg);
			printf("soy el proceso hijo %d\n", pid);
		}
		else if(pid > 0)
		{
			do_nothing_random(msg);
			printf("soy el proceso padre %d\n", pid);
			int status;
			wait(&status);
		}
		else
		{
			printf("\nHubo un error");
		}
		i++;
	}
	return 0;
}


