#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

void do_nothing(int microsegundos, char* mensaje)
{
	usleep(microsegundos);
	printf("\n %s \n", mensaje);
}

void do_nothing_random(char * mensaje)
{
	srand(time(NULL));
	int microsegundos = rand() % 1000 + 1;
	usleep(microsegundos);
	printf("\n %s \n", mensaje);
}

int main()
{
	char* msg = "Hola ";
	pid_t pid;

	int i = 0;
	while(i < 1)
	{
		pid = fork();
		
		if(pid = 0)
		{
			do_nothing(20000, msg);
			printf("soy el proceso hijo %d\n", pid);
		}
		else if(pid > 0)
		{
			do_nothing_random(msg);
			printf("Soy el proceso padre %d\n", pid);
		}	
		else
		{
			printf("\nHubo un error");
		}
		i++;
	}
	return 0;
}
