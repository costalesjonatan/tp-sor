#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

void do_nothing(int microsegundos, char* mensaje)
{
	usleep(microsegundos);
	printf("\n %s ", mensaje);
}

void do_nothing_random(char * mensaje)
{
	srand(time(NULL));
	int microsegundos = rand() % 1000 + 1;
	usleep(microsegundos);
	printf("\n %s \n", mensaje);
}

int main()
{	
	char* msg = "Hola";
	do_nothing(2000000, msg);
	do_nothing_random(msg);
}


