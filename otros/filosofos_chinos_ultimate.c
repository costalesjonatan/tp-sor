# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <pthread.h>
#include <semaphore.h>

static void *ciclo(void* i);
static void pensar(int id);
static void comer(int id, int izquierda, int derecha);
static void agarrarPalillos(int id, int izquierda, int derecha);
static void comprobar(int id, int izquierda, int derecha);
static void do_nothing(int microsegundos, char* mensaje);

sem_t mutex;
sem_t palillos[5];

int estado[5]; //0 = pensado, 1 = hambriento, 2 = comiendo 

char* msg1 = "Eureka!";
char* msg2 = "Provecho!";

int main()
{
	int id[5];
	sem_init(&mutex,0,1);
	pthread_t filosofo[5];
	
	for(int i = 0; i < 5; i++)
	{
		sem_init(&palillos[i],0,1);
		estado[i] = 0;
	}

	for(int i = 0; i < 5; i++)
	{
		id[i] = i;
		pthread_create(&filosofo[i], NULL, *ciclo, (void *) &id[i]);
	}
	
	for(int i = 0; i < 5; i++)
	{
		pthread_join(filosofo[i], NULL);
	}
	
	return 0;
}

void *ciclo(void* i)
{
	int id = *(int *) i;

	int palilloIzquierdo, palilloDerecho;

	if(id == 0)
	{
		palilloIzquierdo = 4;
		palilloDerecho = id + 1;
	}
	else
	{
		palilloDerecho = id - 1;
		palilloIzquierdo = id + 1;
	}

	while(1)
	{
		pensar(id);
		agarrarPalillos(id, palilloIzquierdo, palilloDerecho);
		comer(id, palilloIzquierdo, palilloDerecho);
	}
}

void pensar(int id)
{
	printf("Soy el filosofo %d y estoy pensando...\n", id+1);
	do_nothing(2000000, msg1);
	printf(" soy el filosofo %d\n", id+1);
	estado[id] = 1;
}

void comer(int id, int izquierda, int derecha)
{
	estado[id] = 2;
	printf("Soy el filosofo %d y estoy comiendo...\n", id+1);
	do_nothing(2000000, msg2);
	printf("soy el filosofo %d\n", id+1);
	sem_wait(&mutex);
	sem_post(&palillos[izquierda]);
	sem_post(&palillos[derecha]);
	sem_post(&mutex);
}

void do_nothing(int microsegundos, char* mensaje)
{
	usleep(microsegundos);
	printf(" %s ", mensaje);
}

void agarrarPalillos(int id, int izquierda, int derecha)
{
	sem_wait(&mutex);
	comprobar(id, izquierda, derecha);
	printf("Soy el filosofo %d y estoy agarrando el palillo izquierdo...\n", id+1);
	printf("Soy el filosofo %d y estoy agarrando el palillo derecho...\n", id+1);
}

void comprobar(int id, int izquierda, int derecha)
{
	if(estado[id] == 1 && estado[izquierda] != 2 && estado[derecha] != 2)
	{
		sem_wait(&palillos[izquierda]);
		sem_wait(&palillos[derecha]);
	}
}

