# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <pthread.h>
#include <semaphore.h>

static void *ciclo(void* i);
static void pensar(int id);
static void comer(int id);
static void agarrarPalillos(int id);
static void comprobar(int id);
static void do_nothing(int microsegundos, char* mensaje);

sem_t mutex;
sem_t semaforos[5];

int estado[5]; //0 = pensado, 1 = hambriento, 2 = comiendo 

char* msg1 = "Eureka!";
char* msg2 = "Provecho!";

int main()
{
	int id[5];
	sem_init(&mutex,0,1);
	pthread_t filosofo[5];
	
	for(int i = 0; i < 5; i++)
	{
		sem_init(&semaforos[i],0,0);
		estado[i] = 0;
	}

	for(int i = 4; i >= 0; i--)
	{
		id[i] = i;
		pthread_create(&filosofo[i], NULL, *ciclo, (void *) &id[i]);
	}
	
	for(int i = 0; i < 5; i++)
	{
		pthread_join(filosofo[i], NULL);
	}
	
	return 0;
}

void *ciclo(void* i)
{
	int id = *(int *) i;

	while(1)
	{
		pensar(id);
		agarrarPalillos(id);
		comer(id);
	}
}

void pensar(int id)
{
	printf("Soy el filosofo %d y estoy pensando...\n", id+1);
	do_nothing(2000000, msg1);
	printf(" soy el filosofo %d\n", id+1);
}

void comer(int id)
{
	printf("Soy el filosofo %d y estoy comiendo...\n", id+1);
	do_nothing(2000000, msg2);
	printf("soy el filosofo %d\n", id+1);
	sem_wait(&mutex);
	int izquierda, derecha;

	if(id == 1)
	{
		derecha = 4;
		izquierda = id + 1;
	}
	else
	{
		derecha = id - 1;
		izquierda = id + 1;
	}
	estado[id] = 0;
	comprobar(izquierda);
	comprobar(derecha);
	sem_post(&mutex);
}

void do_nothing(int microsegundos, char* mensaje)
{
	usleep(microsegundos);
	printf(" %s ", mensaje);
}

void agarrarPalillos(int id)
{
	sem_wait(&mutex);//le doy acceso exclusivo para que agarrae los palillos y coma
	printf("soy el filosofo %d y estoy agarrando los palillos..\n", id+1);
	estado[id] = 1;
	comprobar(id); //comprueba que el filosofo de su izquierda y su derecha no esten comiendo
	sem_post(&mutex);//permito que otro filosofo intente agarrar los palillos
	sem_wait(&semaforos[id]); //si comprobar no libera se va a quedar esperando aca
}

void comprobar(int id)
{
	int izquierda, derecha;

	if(id == 1)
	{
		derecha = 4;
		izquierda = id + 1;
	}
	else
	{
		derecha = id - 1;
		izquierda = id + 1;
	}

	if(estado[id] == 1 && estado[derecha] != 2 && estado[izquierda]!= 2)
	{
		estado[id] = 2;
		sem_post(&semaforos[id]); //libero al filosofo, puedo agarrar los palillos
	}
}

