#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>

void do_nothing(int microsegundos, char* mensaje)
{
	usleep(microsegundos);
	printf("\n %s ", mensaje);
}

void do_nothing_random(char * mensaje)
{
	srand(time(NULL));
	int microsegundos = rand() % 1000 + 1;
	usleep(microsegundos);
	printf("\n %s ", mensaje);
}

int main(int argc, char *argv[])
{
	pid_t pid;
	int i = 0;
	char* msg = "Hola ";

	pid = fork();

	if(pid == 0)
	{
		msg = "Hola soy el proceso hijo.";
		do_nothing(2000000, msg);
		do_nothing(2000000, msg);
		do_nothing(2000000, msg);
		do_nothing(2000000, msg);
		do_nothing(2000000, msg);
		do_nothing(2000000, msg);
		do_nothing(2000000, msg);
		do_nothing(2000000, msg);
		do_nothing(2000000, msg);
		do_nothing(2000000, msg);
	}
	else if(pid > 0)
	{
		msg = "Hola soy el proceso padre.";
		do_nothing_random(msg);
		do_nothing_random(msg);
		do_nothing_random(msg);
		do_nothing_random(msg);
		do_nothing_random(msg);
		do_nothing_random(msg);
		do_nothing_random(msg);
		do_nothing_random(msg);
		do_nothing_random(msg);
		do_nothing_random(msg);
	}
	else
	{
		printf("\nHubo un error");
	}

	int status;
	wait(&status);

	return 0;
}


