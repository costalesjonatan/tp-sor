
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <pthread.h>
#include <semaphore.h> //libreria

static void *ciclo(void *arg);
static void pensar(int filosofo);
static void comer(int filosofo);
static void do_nothing(int seg, char* mensaje);

sem_t palillo1;
sem_t palillo2;
sem_t palillo3;
sem_t palillo4;
sem_t palillo5;

int numero_filosofo;
int ciclos;

char* msg1;
char* msg2;

static void* ciclo(void* arg){

	int filosofo = numero_filosofo++; 

	while(ciclos < 15){
		pensar(filosofo);
		if(filosofo == 1)
		{
			sem_wait(&palillo5);
			sem_wait(&palillo1);
			comer(filosofo);
			sem_post(&palillo1);
			sem_post(&palillo5);
			ciclos++;
		}
		else if(filosofo == 2)
		{
			sem_wait(&palillo1);
			sem_wait(&palillo2);
			comer(filosofo);
			sem_post(&palillo2);
			sem_post(&palillo1);
			ciclos++;
		}	
		else if(filosofo == 3)
		{
			sem_wait(&palillo2);
			sem_wait(&palillo3);
			comer(filosofo);
			sem_post(&palillo3);
			sem_post(&palillo2);
			ciclos++;
		}
		else if(filosofo == 4)
		{
			sem_wait(&palillo3);
			sem_wait(&palillo4);
			comer(filosofo);
			sem_post(&palillo4);
			sem_post(&palillo3);
			ciclos++;
		}
		else if(filosofo == 5)
		{
			sem_wait(&palillo4);
			sem_wait(&palillo5);
			comer(filosofo);
			sem_post(&palillo5);
			sem_post(&palillo4);
			ciclos++;
		}
	}
}

void pensar(int filosofo)
{
	printf("Soy el filosofo %d: Pensando...\n", filosofo);
	do_nothing(2, msg1);
	printf(", soy el filosofo %d\n", filosofo);
}

void do_nothing(int seg, char* mensaje)
{
	int microsegundos = seg * 1000000;
	usleep(microsegundos);
	printf("\n%s", mensaje);
}

void comer(int filosofo)
{
	printf("Soy el filosofo %d y estoy agarrando el palillo izquierdo\n", filosofo);
	printf("Soy el filosofo %d y estoy agarrando el palillo derecho\n", filosofo);
	printf("Comiendo, soy el filosofo %d", filosofo);
	do_nothing(2, msg2);
	printf(", soy el filosofo %d\n", filosofo);
}

int main (void)
{
	msg1 = "Eureka!";
	msg2 = "Provecho!";
	numero_filosofo = 1;
	ciclos = 0;

	sem_init(&palillo1,0,1);
	sem_init(&palillo2,0,1);
	sem_init(&palillo3,0,1);
	sem_init(&palillo4,0,1);
	sem_init(&palillo5,0,1);

	pthread_t  pthread_filosofo_1, pthread_filosofo_2, pthread_filosofo_3, pthread_filosofo_4, pthread_filosofo_5;

        pthread_create(&pthread_filosofo_1, NULL, *ciclo, NULL);
	pthread_create(&pthread_filosofo_2, NULL, *ciclo, NULL);
	pthread_create(&pthread_filosofo_3, NULL, *ciclo, NULL);
	pthread_create(&pthread_filosofo_4, NULL, *ciclo, NULL);
	pthread_create(&pthread_filosofo_5, NULL, *ciclo, NULL);
	
        pthread_join(pthread_filosofo_1, NULL);
        pthread_join(pthread_filosofo_2, NULL);
	pthread_join(pthread_filosofo_3, NULL);
	pthread_join(pthread_filosofo_4, NULL);
	pthread_join(pthread_filosofo_5, NULL);
	
	sem_destroy(&palillo1);
	sem_destroy(&palillo2);
	sem_destroy(&palillo3);
	sem_destroy(&palillo4);
	sem_destroy(&palillo5);

	return 0;
}


